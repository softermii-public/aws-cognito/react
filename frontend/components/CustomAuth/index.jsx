import React from 'react';
import { Authenticator } from '@aws-amplify/ui-react';
import { Amplify } from 'aws-amplify';
import '@aws-amplify/ui-react/styles.css';

const AuthTypes = {
    'password': 'password'
};
const CustomAuth = ({ type, formFields, components, innerContent, awsExports }) => {
    Amplify.configure(awsExports);

    switch (type) {
        case 'password':
        {
            return (
                <Authenticator
                formFields={formFields}
                components={components}
                hideSignUp={true}
                >
                    {({ signOut, user }) => (
                        {innerContent}
                    )}
                </Authenticator>);
        }
        case 'passwordLess':
        {
            return (
                <div>Put here custom ui with needed fields, set up in Amplify</div>
            );
        }
        default:
        {
            return (
                <div>Put here custom ui with needed fields, set up in Amplify</div>
            );
        }
    }
};


export default CustomAuth;
